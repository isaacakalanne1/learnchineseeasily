//
//  ArrayFactory.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 19/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "ArrayFactory.h"
#import "StringConstants.h"

@implementation ArrayFactory

+ (NSArray *) finalsAArray {
    return [NSArray arrayWithObjects:@"a", @"ai", @"ao", @"an", @"ang", nil];
}

+ (NSArray *) finalsOArray {
    return [NSArray arrayWithObjects:@"o", @"ou", @"ong", nil];
}

+ (NSArray *) finalsEArray {
    return [NSArray arrayWithObjects:@"e", @"ei", @"en", @"eng", @"er", nil];
}

+ (NSArray *) finalsIArray {
    return [NSArray arrayWithObjects:@"i", @"ia", @"iao", @"ian", @"iang", @"ie", @"iu", @"in", @"ing", @"iong", nil];
}

+ (NSArray *) finalsUArray {
    return [NSArray arrayWithObjects:@"u", @"ua", @"uai", @"uan", @"uang", @"uo", @"ui", @"un", nil];
}

+ (NSArray *) finalsUUArray {
    return [NSArray arrayWithObjects:@"ü", @"üe", @"üan", @"ün", nil];
}

+ (NSArray *) allGroupedFinals {
    return [NSArray arrayWithObjects:ArrayFactory.finalsAArray, ArrayFactory.finalsOArray, ArrayFactory.finalsEArray, ArrayFactory.finalsIArray, ArrayFactory.finalsUArray, ArrayFactory.finalsUUArray, nil];
}

+ (NSArray *) initials1Array {
    return [NSArray arrayWithObjects:@"b", @"p", @"m", @"f", nil];
}

+ (NSArray *) initials2Array {
    return [NSArray arrayWithObjects:@"d", @"t", @"n", @"l", nil];
}

+ (NSArray *) initials3Array {
    return [NSArray arrayWithObjects:@"g", @"k", @"h", nil];
}

+ (NSArray *) initials4Array {
    return [NSArray arrayWithObjects:@"j", @"q", @"x", nil];
}

+ (NSArray *) initials5Array {
    return [NSArray arrayWithObjects:@"z", @"c", @"s", nil];
}

+ (NSArray *) initials6Array {
    return [NSArray arrayWithObjects:@"zh", @"ch", @"sh", @"r", nil];
}

+ (NSArray *) initials7Array {
    return [NSArray arrayWithObjects:@"y", @"w", nil];
}

+ (NSArray *) allGroupedInitials {
    return [NSArray arrayWithObjects:ArrayFactory.initials1Array, ArrayFactory.initials2Array, ArrayFactory.initials3Array, ArrayFactory.initials4Array, ArrayFactory.initials5Array, ArrayFactory.initials6Array, ArrayFactory.initials7Array, nil];
}

// TODO Fix kuài audio file not existing

+ (NSArray *) getWordsForCategory:(NSString *)category andHSKLevel:(NSString *)hskLevel {
    NSArray *arrayofAllCategoryObjects = [ArrayFactory getAllDataForHSKLevel:hskLevel];
    NSArray *arrayOfWordsForCategory = [[NSArray alloc] init];
    for (int i = 0; i < arrayofAllCategoryObjects.count; i++) {
        NSDictionary *categoryObject = [arrayofAllCategoryObjects objectAtIndex:i];
        if ([[categoryObject objectForKey:CATEGORY_KEY] isEqualToString:category]) {
            arrayOfWordsForCategory = [categoryObject objectForKey:ARRAY_OF_WORDS_KEY];
        }
    }
    return arrayOfWordsForCategory;
}

+ (NSArray *) listOfCategoryNamesForHskLevel:(NSString *)hskLevel {
    NSArray *listOfCategoryObjects = [ArrayFactory getAllDataForHSKLevel:hskLevel];
    NSMutableArray *listOfCategoryNames = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < listOfCategoryObjects.count; i++) {
        NSDictionary *categoryObject = [listOfCategoryObjects objectAtIndex:i];
        NSString *categoryName = [categoryObject objectForKey:CATEGORY_KEY];
        [listOfCategoryNames addObject:categoryName];
    }
    return listOfCategoryNames;
}

+ (NSMutableArray *) getAllDataForHSKLevel:(NSString *)hskTitle {
    hskTitle = [hskTitle lowercaseString];
    NSString *pathOfVocabularyPlist = [[NSBundle mainBundle] pathForResource:hskTitle ofType:PLIST_TYPE];
    NSMutableArray *arrayOfVocabularyItems = [[NSMutableArray alloc] initWithContentsOfFile:pathOfVocabularyPlist];
    return arrayOfVocabularyItems;
}

+ (NSMutableArray *) getAllPracticeWordsForList:(NSString *)hskTitle {
    NSMutableArray *arrayOfAllPracticeWords = [[NSMutableArray alloc] init];
    NSMutableArray *arrayOfPracticeCategories = [[NSMutableArray alloc] init];
    NSUserDefaults *savedData = [NSUserDefaults standardUserDefaults];
    if ([hskTitle isEqualToString:HSK1_PRACTICE_LIST_KEY]) {
        arrayOfPracticeCategories = [NSMutableArray arrayWithArray:[savedData objectForKey:HSK1_PRACTICE_LIST_KEY]];
        for (int i = 0 ; i < arrayOfPracticeCategories.count ; i++) {
            NSDictionary *categoryObject = [arrayOfPracticeCategories objectAtIndex:i];
            NSMutableArray *arrayOfWordsInCategory = [[NSMutableArray alloc] init];
            arrayOfWordsInCategory = [NSMutableArray arrayWithArray:[categoryObject objectForKey:ARRAY_OF_WORDS_KEY]];
            for (int n = 0 ; n < arrayOfWordsInCategory.count ; n++) {
                NSDictionary *practiceWord = [arrayOfWordsInCategory objectAtIndex:n];
                BOOL isPracticing = [[practiceWord objectForKey:IS_PRACTICING_KEY] boolValue];
                if (isPracticing) {
                    [arrayOfAllPracticeWords addObject:practiceWord];
                }
            }
        }
    }
    return arrayOfAllPracticeWords;
}

+ (NSMutableArray *)shuffledArray:(NSMutableArray *)array {
    for (NSUInteger i = 0; i < array.count - 1; ++i) {
        NSInteger remainingCount = array.count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    return array;
}

+ (NSArray *) hskComingSoon {
    return [NSArray arrayWithObjects:@"Coming Soon!", nil];
}

+ (NSArray *) listOfHSKLevels {
    return [NSArray arrayWithObjects:HSK_HEADER_STRING, HSK1_STRING, HSK2_STRING, HSK3_STRING, HSK4_STRING, HSK5_STRING, HSK6_STRING, nil];
}

+ (NSArray *) arrayOfBackgroundURLs {
    return [NSArray arrayWithObjects:@"https://www.ft.com/__origami/service/image/v2/images/raw/https%3A%2F%2Fs3-ap-northeast-1.amazonaws.com%2Fpsh-ex-ftnikkei-3937bb4%2Fimages%2F9%2F3%2F5%2F2%2F20112539-1-eng-GB%2F0297123.jpg?source=nar-cms.jpg", @"https://mfiles.alphacoders.com/581/581810.jpg", @"https://i.pinimg.com/originals/7e/52/15/7e5215e093161256a5dff83667a33b1a.jpg", @"https://cdn.wallpapersafari.com/35/48/bmlzB9.jpg", @"https://cdn.hipwallpaper.com/i/74/73/iQezCU.jpg", @"http://thecorner.eu/wp-content/uploads/2016/05/China_recurso.jpg", @"https://i.pinimg.com/originals/e7/13/c7/e713c790e31e2d0c1042c557e35afa24.jpg", @"https://wallpaperaccess.com/full/348583.jpg", @"http://eskipaper.com/images/china-4.jpg", nil];
}

@end
