//
//  ArrayFactory.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 19/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArrayFactory : NSObject

+ (NSArray *) finalsAArray;
+ (NSArray *) finalsOArray;
+ (NSArray *) finalsEArray;
+ (NSArray *) finalsIArray;
+ (NSArray *) finalsUArray;
+ (NSArray *) finalsUUArray;
+ (NSArray *) allGroupedFinals;

+ (NSArray *) initials1Array;
+ (NSArray *) initials2Array;
+ (NSArray *) initials3Array;
+ (NSArray *) initials4Array;
+ (NSArray *) initials5Array;
+ (NSArray *) initials6Array;
+ (NSArray *) initials7Array;
+ (NSArray *) allGroupedInitials;

+ (NSArray *) getWordsForCategory:(NSString *)category andHSKLevel:(NSString *)hskLevel;
+ (NSMutableArray *) getAllDataForHSKLevel:(NSString *)hskTitle;
+ (NSMutableArray *) getAllPracticeWordsForList:(NSString *)hskTitle;
+ (NSArray *) listOfCategoryNamesForHskLevel:(NSString *)hskLevel;
+ (NSMutableArray *)shuffledArray:(NSMutableArray *)array;

+ (NSArray *) hskComingSoon;
+ (NSArray *) listOfHSKLevels;

+ (NSArray *) arrayOfBackgroundURLs;

@end
