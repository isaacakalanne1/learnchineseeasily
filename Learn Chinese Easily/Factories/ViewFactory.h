//
//  ViewManager.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 06/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewFactory : NSObject

+ (UIView *) createWhiteBoxWithShadow;
+ (UIButton *) createMainButtonWithTitle:(NSString *)titleString andFrame:(CGRect)buttonFrame;
+ (UIButton *) createSecondaryButton;
+ (UIImageView *) createBackgroundImageView;
+ (UITextField *) createEditTextField;
+ (UIImage *) getBackgroundImageAtIndex:(NSInteger)index;
+ (CALayer *) setLargeShadow;
+ (UIImage *) getImageFromUrl:(NSString *)urlString;
+ (UILabel *) createEmptyDefaultLabelWithFrame:(CGRect)frame;
+ (UILabel *) createDefaultLabelWithText:(NSString *)text andFrame:(CGRect)frame;
+ (UILabel *) createHeaderLabelWithText:(NSString *)text andFrame:(CGRect)frame;
+ (float) getBottom:(UIView *)view;
+ (float) getBottomOfFrame:(CGRect)frame;
+ (float) getNavigationBarHeight:(UIViewController *)viewController;
+ (float) getTabBarHeight:(UIViewController *)viewController;
+ (float) finalsCellLabelWidth;
+ (float) initialsCellLabelWidth;
+ (float) cellLabelHeight;
+ (float) cellMargin;
+ (float) finalsCellWidth;
+ (float) initialsCellWidth;
+ (float) cellHeight;
+ (UIButton *) createPronounciationButtonWithTitle:(NSString *)titleString;

@end
