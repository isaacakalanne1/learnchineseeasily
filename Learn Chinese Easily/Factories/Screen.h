//
//  SizeFactory.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 21/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Screen : NSObject

+ (float) width;
+ (float) height;
+ (CGRect) rect;
+ (float) centreX;
+ (CGRect) statusBarRect;

@end
