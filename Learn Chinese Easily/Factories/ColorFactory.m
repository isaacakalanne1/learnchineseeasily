//
//  ColorManager.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 06/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "ColorFactory.h"

@implementation ColorFactory

+ (UIColor *) mainColor {
    return [UIColor colorWithRed:0.812 green:0.282 blue:0.176 alpha:1.0];
}

@end
