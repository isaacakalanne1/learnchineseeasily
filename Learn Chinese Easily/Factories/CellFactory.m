//
//  CellFactory.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 23/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "WordTableViewCell.h"

#import "CellFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"

@implementation CellFactory

+ (UITableViewCell *) createHeaderCell:(UITableViewCell *)cell withText:(NSString *)text {
    cell.textLabel.text = text;
    cell.backgroundColor = ColorFactory.mainColor;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = FontFactory.mainFontSmall;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

+ (WordTableViewCell *) createNormalCell:(WordTableViewCell *)cell withText:(NSString *)text {
    cell.textLabel.text = text;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = ColorFactory.mainColor;
    cell.textLabel.font = FontFactory.mainFontMedium;
    return cell;
}

@end
