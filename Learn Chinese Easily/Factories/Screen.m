//
//  SizeFactory.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 21/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "Screen.h"

@implementation Screen

+ (float) width {
    return [[UIScreen mainScreen] bounds].size.width;
}

+ (float) height {
    return [[UIScreen mainScreen] bounds].size.height;
}

+ (CGRect) rect {
    return [[UIScreen mainScreen] bounds];
}

+ (float) centreX {
    return [[UIScreen mainScreen] bounds].size.width/2;
}

+ (CGRect) statusBarRect {
    return [[UIApplication sharedApplication] statusBarFrame];
}

@end
