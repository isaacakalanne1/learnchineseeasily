//
//  ViewManager.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 06/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "ViewFactory.h"
#import "ArrayFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"
#import "ViewFactory.h"
#import "Screen.h"
#import "StringConstants.h"

NSInteger numberOfFinalsColumns = 6;
NSInteger numberOfInitialsColumns = 7;

@implementation ViewFactory

+ (UIView *) createWhiteBoxWithShadow {
    UIView *shadowView = [[UIView alloc] init];
    shadowView.backgroundColor = [UIColor whiteColor];
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowRadius = 15;
    shadowView.layer.shadowOpacity = 0.5;
    shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    return shadowView;
}

+ (UIButton *) createMainButtonWithTitle:(NSString *)titleString andFrame:(CGRect)buttonFrame {
    UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
    button.backgroundColor = [UIColor whiteColor];
    button.layer.borderColor = [ColorFactory mainColor].CGColor;
    button.layer.borderWidth = 1;
    button.layer.cornerRadius = button.frame.size.height / 2;
    button.titleLabel.font = FontFactory.mainFontLarge;
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitleColor:[ColorFactory mainColor] forState:UIControlStateNormal];
    return button;
}

+ (UIButton *) createSecondaryButton {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    button.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    button.titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    button.titleLabel.layer.shadowOpacity = 1;
    button.titleLabel.layer.shadowRadius = 3;
    button.titleLabel.font = FontFactory.mainFontSmall;
    [button setTitle:EDIT_PRACTICE_LIST forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return button;
}

+ (UIImageView *) createBackgroundImageView {
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(0, 0, Screen.width, Screen.height);
    imageView.alpha = 1;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    return imageView;
}

+ (UITextField *) createEditTextField {
    UITextField *textView = [[UITextField alloc] init];
    textView.frame = CGRectMake(0, Screen.height - 120, Screen.width, 100);
    textView.backgroundColor = ColorFactory.mainColor;
    textView.textColor = UIColor.whiteColor;
    textView.textAlignment = NSTextAlignmentCenter;
    return textView;
}

+ (UILabel *) createEmptyDefaultLabelWithFrame:(CGRect)frame {
    UILabel *label = [self createDefaultLabelWithText:EMPTY_STRING andFrame:frame];
    return label;
}

+ (UILabel *) createDefaultLabelWithText:(NSString *)text andFrame:(CGRect)frame {
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textColor = ColorFactory.mainColor;
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:FontFactory.mainFontMedium];
    label.numberOfLines = 0;
    return label;
}

+ (UILabel *) createHeaderLabelWithText:(NSString *)text andFrame:(CGRect)frame {
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textColor = UIColor.whiteColor;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = FontFactory.mainFontSmall;
    label.backgroundColor = ColorFactory.mainColor;
    return label;
}

- (void) defaultSelector {
    NSLog(@"Default selector called");
}

+ (UIImage *) getBackgroundImageAtIndex:(NSInteger)index {
    return [self getImageFromUrl:[ArrayFactory.arrayOfBackgroundURLs objectAtIndex:index]];
}

+ (CALayer *) setLargeShadow {
    CALayer *layer;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowRadius = 15;
    layer.shadowOpacity = 0.5;
    layer.shadowOffset = CGSizeMake(0, 0);
    return layer;
}

+ (UIImage *) getImageFromUrl:(NSString *)urlString {
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
}

+ (float) getBottom:(UIView *)view {
    return [self getBottomOfFrame:view.frame];
}

+ (float) getBottomOfFrame:(CGRect)frame {
    return frame.origin.y + frame.size.height;
}

+ (float) getNavigationBarHeight:(UIViewController *)viewController {
    return viewController.navigationController.navigationBar.frame.size.height;
}

+ (float) getTabBarHeight:(UIViewController *)viewController {
    return viewController.tabBarController.tabBar.frame.size.height;
}

+ (float) finalsCellLabelWidth {
    return ViewFactory.finalsCellWidth - ViewFactory.cellMargin * 2;
}
+ (float) initialsCellLabelWidth {
    return ViewFactory.initialsCellWidth - ViewFactory.cellMargin * 2;
}

+ (float) cellLabelHeight {
    return ViewFactory.cellHeight - ViewFactory.cellMargin * 2;
}

+ (float) cellMargin {
    return 4;
}

+ (float) finalsCellWidth {
    return Screen.width / numberOfFinalsColumns;
}

+ (float) initialsCellWidth {
    return Screen.width / numberOfInitialsColumns;
}

+ (float) cellHeight {
    return 45;
}

+ (UIButton *) createPronounciationButtonWithTitle:(NSString *)titleString {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitleColor:ColorFactory.mainColor forState:UIControlStateNormal];
    [button.titleLabel setFont:FontFactory.mainFontMedium];
    return button;
}

@end
