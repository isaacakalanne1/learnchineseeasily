//
//  CellFactory.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 23/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordTableViewCell.h"
#import <UIKit/UIKit.h>

@interface CellFactory : NSObject

+ (UITableViewCell *) createHeaderCell:(UITableViewCell *)cell withText:(NSString *)text;
+ (WordTableViewCell *) createNormalCell:(UITableViewCell *)cell withText:(NSString *)text;

@end
