//
//  FontFactory.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 30/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "FontFactory.h"
#import "StringConstants.h"

@implementation FontFactory

+ (UIFont *) mainFontLarge {
    return [UIFont fontWithName:AVENIR_FONT size:24];
}

+ (UIFont *) mainFontMedium {
    return [UIFont fontWithName:AVENIR_FONT size:20];
}

+ (UIFont *) mainFontSmall {
    return [UIFont fontWithName:AVENIR_FONT size:16];
}

@end
