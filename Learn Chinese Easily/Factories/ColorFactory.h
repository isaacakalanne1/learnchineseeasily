//
//  ColorManager.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 06/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorFactory : NSObject

+ (UIColor *) mainColor;

@end
