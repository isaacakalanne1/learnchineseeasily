//
//  PracticeViewController.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 11/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <GameplayKit/GameplayKit.h>

@interface PracticeViewController : UIViewController <UITextFieldDelegate, AVAudioPlayerDelegate>

@property (strong) UITextField *inputField;
@property (strong) UILabel *pinyinLabel;
@property (strong) UILabel *hanziLabel;
@property (strong, nonatomic) NSDictionary *wordData;
@property (strong, nonatomic) AVAudioPlayer *practiceWordAudioPlayer;

@property (nonatomic) NSInteger imageIndex;

@end
