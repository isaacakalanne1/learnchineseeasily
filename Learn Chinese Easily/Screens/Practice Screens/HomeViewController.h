//
//  FirstViewController.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 14/09/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

- (NSInteger) randomIndex;

@end

