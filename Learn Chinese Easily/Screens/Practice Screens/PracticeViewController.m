//
//  PracticeViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 11/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "PracticeViewController.h"
#import "ViewFactory.h"
#import "ColorFactory.h"
#import "ArrayFactory.h"
#import "FontFactory.h"
#import "StringConstants.h"
#import "SizeConstants.h"
#import "Screen.h"

@interface PracticeViewController ()

@end

int indexOfCurrentWord;
bool isShowingCorrectAnswer;

float widthOfCounterLabel = 100;
float diameterOfFlashCard = 150;
float topMarginOfLabel = 100;
float widthOfAnswerStatusLabel = 70;

UILabel *questionNumberLabel;

UILabel *wrongAnswerCounter;
int numberOfWrongAnswers;

UILabel *rightAnswerCounter;
int numberOfRightAnswers;

UILabel *_definitionLabel;
UILabel *answerStatusLabel;
UIView *flashCardBackground;

NSMutableArray *arrayOfPracticeWords;

UIButton *showAnswerButton;

@implementation PracticeViewController

@synthesize wordData;
@synthesize inputField;
@synthesize pinyinLabel;
@synthesize hanziLabel;
@synthesize practiceWordAudioPlayer;
@synthesize imageIndex;

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self initialiseVariables];
    arrayOfPracticeWords = [self createAndShuffleArrayOfWords];
    [self createInterface];
    [self gotoNextPracticeWord];
    [self addKeyboardObserver];
}

- (void) initialiseVariables {
    indexOfCurrentWord = -1;
    numberOfWrongAnswers = 0;
    numberOfRightAnswers = 0;
    isShowingCorrectAnswer = false;
}

- (NSMutableArray *) createAndShuffleArrayOfWords {
    
    NSMutableArray *array = [ArrayFactory getAllPracticeWordsForList:HSK1_PRACTICE_LIST_KEY];
    if (array.count == 0) {
        NSLog(@"It's empty!");
        array = [self getAllWordsForList:HSK1_PRACTICE_LIST_KEY];
    }
    return [ArrayFactory shuffledArray:array];
}

- (NSMutableArray *) getAllWordsForList:(NSString *)listTitle {
    NSMutableArray *arrayOfAllWords = [[NSMutableArray alloc] init];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:HSK1_PATH ofType:PLIST_TYPE];
    NSMutableArray *allCategoryObjects = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    for (int i = 0 ; i < allCategoryObjects.count ; i++) {
        NSDictionary *categoryObject = [allCategoryObjects objectAtIndex:i];
        NSArray *arrayOfWordsInCategory = [categoryObject objectForKey:ARRAY_OF_WORDS_KEY];
        [arrayOfAllWords addObjectsFromArray:arrayOfWordsInCategory];
    }
    return arrayOfAllWords;
}

#pragma mark App Logic

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (isShowingCorrectAnswer) {
        if (indexOfCurrentWord < arrayOfPracticeWords.count - 1) {
            [self gotoNextPracticeWord];
        }
    } else {
        [self processUserInputAnswer];
    }
    return YES;
}

- (void) processUserInputAnswer {
    if ([self didInputCorrectAnswer] && !isShowingCorrectAnswer) {
        [self setAnswerStatusLabelToCorrect];
        [self showCorrectAnswer];
    } else {
        [self setAnswerStatusLabelToIncorrect];
    }
}

- (bool) didInputCorrectAnswer {
    return [inputField.text isEqualToString:[wordData objectForKey:HANZI_KEY]];
}

- (void) setAnswerStatusLabelToCorrect {
    numberOfRightAnswers++;
    rightAnswerCounter.text = [self getRightAnswerCounterText];
    answerStatusLabel.text = CHECKMARK;
    answerStatusLabel.textColor = UIColor.greenColor;
    [self.view addSubview:showAnswerButton];
}

- (void) setAnswerStatusLabelToIncorrect {
    numberOfWrongAnswers++;
    wrongAnswerCounter.text = [self getWrongAnswerCounterText];
    answerStatusLabel.text = CROSS;
    answerStatusLabel.textColor = UIColor.redColor;
    [self.view addSubview:showAnswerButton];
}

- (void) showCorrectAnswer {
    isShowingCorrectAnswer = true;
    pinyinLabel.alpha = 1;
    hanziLabel.alpha = 1;
    [self playWordSoundFileWithId:[[wordData objectForKey:ID_KEY] stringValue]];
}

- (void) playWordSoundFileWithId:(NSString *)wordId {
    if (!practiceWordAudioPlayer.isPlaying) {
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:wordId ofType:MP3_TYPE];
        if (soundFilePath != nil) {
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            practiceWordAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [practiceWordAudioPlayer play];
        } else {
            NSLog(FILE_DOESNT_EXIST);
        }
    }
}

- (void) gotoNextPracticeWord {
    [self resetFieldsForNewWord];
    
    [self getDataForNewWord];
    [self updateLabelsForNewWord];
    [self hideCorrectAnswer];
}

- (void) resetFieldsForNewWord {
    inputField.text = EMPTY_STRING;
    answerStatusLabel.text = EMPTY_STRING;
}

- (void) getDataForNewWord {
    indexOfCurrentWord++;
    wordData = [arrayOfPracticeWords objectAtIndex:indexOfCurrentWord];
}

- (void) updateLabelsForNewWord {
    _definitionLabel.text = [wordData objectForKey:ENGLISH_KEY];
    questionNumberLabel.text = [self getQuestionNumberLabelText];
    pinyinLabel.text = [wordData objectForKey:PINYIN_KEY];
    hanziLabel.text = [wordData objectForKey:HANZI_KEY];
}

- (void) hideCorrectAnswer {
    isShowingCorrectAnswer = false;
    pinyinLabel.alpha = 0;
    hanziLabel.alpha = 0;
    [showAnswerButton removeFromSuperview];
}

- (NSString *) getQuestionNumberLabelText {
    return [NSString stringWithFormat:@"%d/%zd", indexOfCurrentWord + 1, arrayOfPracticeWords.count];
}

- (NSString *) getRightAnswerCounterText {
    return [NSString stringWithFormat:@"%d", numberOfRightAnswers];
}

- (NSString *) getWrongAnswerCounterText {
    return [NSString stringWithFormat:@"%d", numberOfWrongAnswers];
}

#pragma mark Interface Creation

- (void) createInterface {
    [self formatNavigationBar];
    [self createMainBackground];
    [self createCounterLabels];
    [self createFlashCardView];
    [self createShowAnswerButton];
    [self createUserInputField];
}

- (void) createCounterLabels {
    [self createCounterLabelsBackground];
    [self createNumberOfWrongAnswersLabel];
    [self createCurrentQuestionNumberLabel];
    [self createNumberOfRightAnswersLabel];
}

- (void) createFlashCardView {
    [self createFlashcardBackground];
    [self createEnglishDefinitionLabel];
    [self createAnswerStatusLabel];
    [self createHanziLabel];
    [self createPinyinLabel];
}

- (void) formatNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    ColorFactory.mainColor, NSForegroundColorAttributeName,
                                                                FontFactory.mainFontMedium, NSFontAttributeName,nil];
}

- (void) createFlashcardBackground {
    flashCardBackground = [ViewFactory createWhiteBoxWithShadow];
    flashCardBackground.frame = CGRectMake(Screen.centreX - widthOfAnswerStatusLabel/2 - diameterOfFlashCard, topMarginOfLabel, diameterOfFlashCard*2 + widthOfAnswerStatusLabel, diameterOfFlashCard);
    [self.view addSubview:flashCardBackground];
}

- (void) createMainBackground {
    UIImageView *background = [ViewFactory createBackgroundImageView];
    background.image = [ViewFactory getBackgroundImageAtIndex:imageIndex];
    [self.view addSubview:background];
}

- (void) createCounterLabelsBackground {
    UIView *countersBackground = [[UIView alloc] init];
    countersBackground.backgroundColor = UIColor.whiteColor;
    countersBackground.frame = CGRectMake(0, 0, Screen.width, LARGE_LABEL_HEIGHT + SMALL_MARGIN * 2);
    [self.view addSubview:countersBackground];
}

- (void) createNumberOfWrongAnswersLabel {
    wrongAnswerCounter = [[UILabel alloc] init];
    wrongAnswerCounter.frame = CGRectMake(SMALL_MARGIN, SMALL_MARGIN, widthOfCounterLabel, LARGE_LABEL_HEIGHT);
    [wrongAnswerCounter setFont:FontFactory.mainFontMedium];
    wrongAnswerCounter.text = [self getWrongAnswerCounterText];
    wrongAnswerCounter.textColor = UIColor.redColor;
    wrongAnswerCounter.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:wrongAnswerCounter];
}

- (void) createCurrentQuestionNumberLabel {
    questionNumberLabel = [[UILabel alloc] init];
    questionNumberLabel.frame = CGRectMake(Screen.centreX - widthOfCounterLabel/2, SMALL_MARGIN, widthOfCounterLabel, LARGE_LABEL_HEIGHT);
    [questionNumberLabel setFont:FontFactory.mainFontMedium];
    questionNumberLabel.text = [self getQuestionNumberLabelText];
    questionNumberLabel.textColor = UIColor.blackColor;
    questionNumberLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:questionNumberLabel];
}

- (void) createNumberOfRightAnswersLabel {
    rightAnswerCounter = [[UILabel alloc] init];
    rightAnswerCounter.frame = CGRectMake(Screen.width - widthOfCounterLabel - SMALL_MARGIN, SMALL_MARGIN, widthOfCounterLabel, LARGE_LABEL_HEIGHT);
    [rightAnswerCounter setFont:FontFactory.mainFontMedium];
    rightAnswerCounter.text = [self getRightAnswerCounterText];
    rightAnswerCounter.textColor = UIColor.greenColor;
    rightAnswerCounter.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:rightAnswerCounter];
}

- (void) createEnglishDefinitionLabel {
    _definitionLabel = [ViewFactory createEmptyDefaultLabelWithFrame:CGRectMake(Screen.centreX - widthOfAnswerStatusLabel/2 - diameterOfFlashCard, topMarginOfLabel, diameterOfFlashCard, diameterOfFlashCard)];
    _definitionLabel.textColor = UIColor.blackColor;
    [self.view addSubview:_definitionLabel];
}

- (void) createAnswerStatusLabel {
    answerStatusLabel = [ViewFactory createEmptyDefaultLabelWithFrame:CGRectMake(Screen.centreX - widthOfAnswerStatusLabel/2, topMarginOfLabel, widthOfAnswerStatusLabel, diameterOfFlashCard)];
    [answerStatusLabel setFont:FontFactory.mainFontLarge];
    [self.view addSubview:answerStatusLabel];
}

- (void) createHanziLabel {
    hanziLabel = [ViewFactory createEmptyDefaultLabelWithFrame:CGRectMake(Screen.centreX + widthOfAnswerStatusLabel/2, topMarginOfLabel + (diameterOfFlashCard/2) - LARGE_LABEL_HEIGHT, diameterOfFlashCard, LARGE_LABEL_HEIGHT)];
    hanziLabel.textColor = UIColor.blackColor;
    [self.view addSubview:hanziLabel];
}

- (void) createPinyinLabel {
    pinyinLabel = [ViewFactory createEmptyDefaultLabelWithFrame:CGRectMake(Screen.centreX + widthOfAnswerStatusLabel/2, topMarginOfLabel + (diameterOfFlashCard/2), diameterOfFlashCard, LARGE_LABEL_HEIGHT)];
    pinyinLabel.textColor = UIColor.blackColor;
    [self.view addSubview:pinyinLabel];
}

- (void) createShowAnswerButton {
    showAnswerButton = [ViewFactory createMainButtonWithTitle:SHOW_ANSWER_STRING andFrame:CGRectMake(Screen.centreX - WIDTH_OF_BUTTON/2, [ViewFactory getBottom:flashCardBackground] + SMALL_MARGIN, WIDTH_OF_BUTTON, HEIGHT_OF_BUTTON)];
    showAnswerButton.titleLabel.font = FontFactory.mainFontSmall;
    showAnswerButton.layer.cornerRadius = showAnswerButton.frame.size.height / 2;
    [showAnswerButton addTarget:self action:@selector(showCorrectAnswer) forControlEvents:UIControlEventTouchUpInside];
}

- (void) createUserInputField {
    inputField = [ViewFactory createEditTextField];
    inputField.frame = CGRectMake(0, [self topOfTabBar] - LARGE_LABEL_HEIGHT, Screen.width, LARGE_LABEL_HEIGHT);
    inputField.delegate = self;
    inputField.returnKeyType = UIReturnKeyDone;
    [self.view addSubview:inputField];
}

#pragma mark Observers and Miscellaneous

- (int) topOfTabBar {
    return Screen.height - self.navigationController.navigationBar.bounds.size.height - self.tabBarController.tabBar.bounds.size.height - Screen.statusBarRect.size.height;
}

- (void) addKeyboardObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
}

- (void) keyboardWillShowNotification:(NSNotification *) notification{
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    float yPosition = [self topOfTabBar] - keyboardFrame.size.height - LARGE_LABEL_HEIGHT;
    if (keyboardFrame.size.height > 0) {
        yPosition += self.navigationController.navigationBar.bounds.size.height;
    }
    [UIView animateWithDuration:1.0 animations:^{
        self->inputField.frame = CGRectMake(0, yPosition, Screen.width, LARGE_LABEL_HEIGHT);
    }];
}

@end
