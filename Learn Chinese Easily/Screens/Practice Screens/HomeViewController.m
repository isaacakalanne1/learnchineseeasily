//
//  FirstViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 14/09/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "HomeViewController.h"
#import "PracticeViewController.h"
#import "ViewFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"
#import "ArrayFactory.h"
#import "SizeConstants.h"
#import "StringConstants.h"
#import "Screen.h"

@interface HomeViewController ()

@end

UIImageView *backgroundImageView;

UIButton *startButton;
float topOfStartButton;

NSInteger imageIndex = 0;

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // TODO - Code in check to compare hsk plist and hsk in NSUserDefaults, to remove and add objects fro categories if words in hsk plist are moved, added, or deleted.
    NSUserDefaults *savedData = [NSUserDefaults standardUserDefaults];
//    [savedData removeObjectForKey:HSK1_PRACTICE_LIST_KEY];
    if ([savedData objectForKey:HSK1_PRACTICE_LIST_KEY] == NULL) {
        NSLog(@"It's null!");
        [savedData setObject:[ArrayFactory getAllDataForHSKLevel:HSK1_PATH] forKey:HSK1_PRACTICE_LIST_KEY];
        [savedData synchronize];
    }
    if ([savedData objectForKey:HSK1_PRACTICE_LIST_KEY] != NULL) {
        NSLog(@"It's been saved!");
    }
    
    [self createSizeVariables];
    [self createInterface];
}

- (void) createInterface {
    [self formatNavigationBar];
    [self formatTabBar];
    [self createViews];
}

- (void) formatNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   ColorFactory.mainColor, NSForegroundColorAttributeName,
                                                                   FontFactory.mainFontMedium, NSFontAttributeName,nil];
    self.navigationController.navigationBar.layer.shadowColor = UIColor.blackColor.CGColor;
    self.navigationController.navigationBar.layer.shadowRadius = SHADOW_RADIUS;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.3;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 0);
}

- (void) formatTabBar {
    self.tabBarController.tabBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabBarController.tabBar.layer.shadowRadius = SHADOW_RADIUS;
    self.tabBarController.tabBar.layer.shadowOpacity = 0.3;
    self.tabBarController.tabBar.layer.shadowOffset = CGSizeMake(0, 0);
}

- (void) createSizeVariables {
    topOfStartButton = self.tabBarController.tabBar.frame.origin.y - self.tabBarController.tabBar.frame.size.height - HEIGHT_OF_BUTTON - 20;
}

- (void) createViews {
    [self createBackground];
    [self createStartButton];
}

- (void) createBackground {
    backgroundImageView = [ViewFactory createBackgroundImageView];
    imageIndex = self.randomIndex;
    backgroundImageView.image = [ViewFactory getBackgroundImageAtIndex:imageIndex];
    [self.view addSubview:backgroundImageView];
}

- (void) createStartButton {
    startButton = [ViewFactory createMainButtonWithTitle:START_STRING andFrame:CGRectMake(Screen.width/2 - WIDTH_OF_BUTTON/2, topOfStartButton, WIDTH_OF_BUTTON, HEIGHT_OF_BUTTON)];
    startButton.layer.cornerRadius = startButton.frame.size.height / 2;
    [startButton addTarget:self action:@selector(openPracticeViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startButton];
}

- (void) openPracticeViewController {
    [self performSegueWithIdentifier:PRACTICE_SEGUE_ID sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:PRACTICE_SEGUE_ID]) {
        PracticeViewController *viewController = [segue destinationViewController];
        [viewController setImageIndex:imageIndex];
    }
}

- (NSInteger) randomIndex {
    NSInteger lowerBound = 0;
    NSInteger upperBound = ArrayFactory.arrayOfBackgroundURLs.count;
    NSInteger randomIndex = lowerBound + arc4random() % (upperBound - lowerBound);
    return randomIndex;
}

@end
