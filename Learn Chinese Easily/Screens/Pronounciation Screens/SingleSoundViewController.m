//
//  SinglePronounciationViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 22/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "SingleSoundViewController.h"
#import "ViewFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"
#import "StringConstants.h"
#import "SizeConstants.h"
#import "Screen.h"

NSDictionary *soundData;

UILabel *titleLabel;
UILabel *hanziLabel;
UILabel *pinyinLabel;
UILabel *definitionLabel;
UILabel *pronounciationLabel;

@interface SingleSoundViewController ()

@end

@implementation SingleSoundViewController

@synthesize soundPlayer;
@synthesize soundString;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    soundData = [self getDictionaryForSound];
    [self createInterface];
    [self playSound];
}

- (void) createInterface {
    [self createTitleLabel];
    [self createHanziLabel];
    [self createPinyinLabel];
    [self createDefinitionLabel];
    [self createPronounciationLabel];
    
    [self createPlayButton];
}

- (void) createTitleLabel {
    titleLabel = [ViewFactory createDefaultLabelWithText:soundString andFrame:CGRectMake(0, MEDIUM_MARGIN, Screen.width, LARGE_LABEL_HEIGHT)];
    titleLabel.font = FontFactory.mainFontLarge;
    [self.view addSubview:titleLabel];
}

- (void) createHanziLabel {
    NSString *hanziString = [soundData objectForKey:HANZI_KEY];
    hanziLabel = [ViewFactory createDefaultLabelWithText:hanziString andFrame:CGRectMake(0, [ViewFactory getBottom:titleLabel] + MEDIUM_MARGIN, Screen.width, MEDIUM_LABEL_HEIGHT)];
    [self.view addSubview:hanziLabel];
}

- (void) createPinyinLabel {
    NSString *pinyinString = [soundData objectForKey:PINYIN_KEY];
    pinyinLabel = [ViewFactory createDefaultLabelWithText:pinyinString andFrame:CGRectMake(0, [ViewFactory getBottom:hanziLabel] + SMALL_MARGIN, Screen.width, MEDIUM_LABEL_HEIGHT)];
    [self.view addSubview:pinyinLabel];
}

- (void) createDefinitionLabel {
    NSString *definitionString = [soundData objectForKey:DEFINITION_KEY];
    definitionLabel = [ViewFactory createDefaultLabelWithText:definitionString andFrame:CGRectMake(0, [ViewFactory getBottom:pinyinLabel] + SMALL_MARGIN, Screen.width, MEDIUM_LABEL_HEIGHT)];
    [self.view addSubview:definitionLabel];
}

- (void) createPronounciationLabel {
    NSString *pronounciationString = [soundData objectForKey:PRONOUNCIATION_KEY];
    pronounciationLabel = [ViewFactory createDefaultLabelWithText:pronounciationString andFrame:CGRectMake(0, [ViewFactory getBottom:definitionLabel] + LARGE_MARGIN, Screen.width, 150)];
    [pronounciationLabel sizeToFit];
    pronounciationLabel.frame = CGRectMake(pronounciationLabel.frame.origin.x, pronounciationLabel.frame.origin.y, Screen.width, pronounciationLabel.frame.size.height);
    [self.view addSubview:pronounciationLabel];
}

- (void) createPlayButton {
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [playButton setTitle:PLAY_SOUND_STRING forState:UIControlStateNormal];
    [playButton setTitleColor:ColorFactory.mainColor forState:UIControlStateNormal];
    [playButton.titleLabel setFont:FontFactory.mainFontMedium];
    [playButton addTarget:self action:@selector(playSound) forControlEvents:UIControlEventTouchUpInside];
    playButton.frame = CGRectMake(0, Screen.height - 150, Screen.width, HEIGHT_OF_BUTTON);
    [self.view addSubview:playButton];
}

- (void) playSound {
    if (!soundPlayer.isPlaying) {
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:soundString ofType:MP3_TYPE];
        if (soundFilePath != nil) {
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [soundPlayer play];
        } else {
            NSLog(FILE_DOESNT_EXIST);
        }
    }
}

- (NSDictionary *) getDictionaryForSound {
    NSString *pathOfSoundsPlist = [[NSBundle mainBundle] pathForResource:PRONOUNCIATION_PATH ofType:PLIST_TYPE];
    NSArray *arrayOfSounds = [[NSArray alloc] initWithContentsOfFile:pathOfSoundsPlist];
    return [self getSoundDataFromArray:arrayOfSounds];
}

- (NSDictionary *) getSoundDataFromArray:(NSArray *)array {
    NSDictionary *dictionary;
    for (int i = 0; i < array.count; i++) {
        NSDictionary *dictionaryOfSound = [array objectAtIndex:i];
        if ([[dictionaryOfSound objectForKey:SOUND_KEY] isEqualToString:soundString]) {
            dictionary = dictionaryOfSound;
            break;
        }
    }
    return dictionary;
}

@end
