//
//  PronounciationViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 14/09/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "AllSoundsViewController.h"
#import "SingleSoundViewController.h"
#import "ColorFactory.h"
#import "ViewFactory.h"
#import "ArrayFactory.h"
#import "FontFactory.h"
#import "StringConstants.h"
#import "SizeConstants.h"
#import "CountConstants.h"
#import "Screen.h"

@interface AllSoundsViewController ()

@end

UIScrollView *scrollView;

UILabel *finalsTitleLabel;
UIView *finalsTable;
UILabel *initialsTitleLabel;
UIView *initialsTable;

NSString *soundString = @"";

@implementation AllSoundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createInterface];
}

- (void) createInterface {
    [self formatNavigationBar];
    
    [self createScrollView];
    [self createTableOfFinals];
    [self createTableOfInitials];
    [self setScrollViewContentSize];
    
    [self addSubviews];
}

- (void) formatNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   ColorFactory.mainColor, NSForegroundColorAttributeName,
                                                                   FontFactory.mainFontMedium, NSFontAttributeName,nil];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowRadius = SHADOW_RADIUS;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.3;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 0);
}

- (void) createScrollView {
    scrollView = [[UIScrollView alloc] init];
    scrollView.frame = CGRectMake(0, 0, Screen.width, Screen.height);
}

- (void) createTableOfFinals {
    finalsTitleLabel = [ViewFactory createHeaderLabelWithText:FINALS_TITLE andFrame:CGRectMake(0, 0, Screen.width, HEADER_CELL_HEIGHT)];
    finalsTable = [[UIView alloc] initWithFrame:CGRectMake(0, [ViewFactory getBottom:finalsTitleLabel], Screen.width, ViewFactory.cellHeight * NUMBER_OF_FINALS_TABLE_ROWS)];
    finalsTable.backgroundColor = UIColor.whiteColor;
    
    [self populateTableOfFinals];
}

- (void) createTableOfInitials {
    initialsTitleLabel = [ViewFactory createHeaderLabelWithText:INITIALS_TITLE andFrame:CGRectMake(0, [ViewFactory getBottom:finalsTable], Screen.width, HEADER_CELL_HEIGHT)];
    initialsTable = [[UIView alloc] initWithFrame:CGRectMake(0, [ViewFactory getBottom:initialsTitleLabel], Screen.width, ViewFactory.cellHeight * NUMBER_OF_INITIALS_TABLE_ROWS)];
    initialsTable.backgroundColor = UIColor.whiteColor;
    
    [self populateTableOfInitials];
}

- (void) setScrollViewContentSize {
    scrollView.contentSize = CGSizeMake(Screen.width, [ViewFactory getBottom:initialsTable] + [ViewFactory getNavigationBarHeight:self] + [ViewFactory getTabBarHeight:self] + Screen.statusBarRect.size.height);
}

- (void) addSubviews {
    [self.view addSubview:scrollView];
    [scrollView addSubview:finalsTitleLabel];
    [scrollView addSubview:finalsTable];
    [scrollView addSubview:initialsTitleLabel];
    [scrollView addSubview:initialsTable];
}

- (void) populateTableOfFinals {
    for (int i = 0; i < ArrayFactory.allGroupedFinals.count; i++) {
        NSArray *groupOfFinals = [ArrayFactory.allGroupedFinals objectAtIndex:i];
        UIView *columnView = [self createColumnAtIndex:i withGroupOfSounds:groupOfFinals forTable:FINALS_TITLE];
        [finalsTable addSubview:columnView];
    }
}

- (void) populateTableOfInitials {
    for (int i = 0; i < ArrayFactory.allGroupedInitials.count; i++) {
        NSArray *groupOfInitials = [ArrayFactory.allGroupedInitials objectAtIndex:i];
        UIView *columnView = [self createColumnAtIndex:i withGroupOfSounds:groupOfInitials forTable:INITIALS_TITLE];
        [initialsTable addSubview:columnView];
    }
}

- (UIView *) createColumnAtIndex:(int) columnIndex withGroupOfSounds:(NSArray *)groupedStrings forTable:(NSString *)tableType {
    NSInteger cellWidth = [self getCellWidthForTable:tableType];
    
    UIView *columnView = [[UIView alloc] initWithFrame:CGRectMake(cellWidth * columnIndex, 0, cellWidth, ViewFactory.cellHeight * groupedStrings.count)];
    for (int i = 0; i < groupedStrings.count; i++) {
        UIButton *button = [self createRowWithIndex:i withTitleString:[groupedStrings objectAtIndex:i] forTable:tableType];
        [columnView addSubview:button];
    }
    return columnView;
}

- (NSInteger) getCellWidthForTable:(NSString *)tableType {
    NSInteger cellWidth = 0;
    if ([tableType isEqualToString:FINALS_TITLE]) {
        cellWidth = ViewFactory.finalsCellWidth;
    } else if ([tableType isEqualToString:INITIALS_TITLE]) {
        cellWidth = ViewFactory.initialsCellWidth;
    }
    return cellWidth;
}

- (UIButton *) createRowWithIndex:(int)rowIndex withTitleString:(NSString *)string forTable:(NSString *)tableType {
    NSInteger cellLabelWidth = [self getCellLabelWidthForTable:tableType];
    
    UIButton *button = [ViewFactory createPronounciationButtonWithTitle:string];
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(ViewFactory.cellMargin, ViewFactory.cellMargin + ViewFactory.cellHeight * rowIndex, cellLabelWidth, ViewFactory.cellLabelHeight);
    [button.layer setValue:string forKey:FINAL_KEY];
    return button;
}

- (NSInteger) getCellLabelWidthForTable:(NSString *)tableType {
    NSInteger cellLabelWidth = 0;
    if ([tableType isEqualToString:FINALS_TITLE]) {
        cellLabelWidth = ViewFactory.finalsCellLabelWidth;
    } else if ([tableType isEqualToString:INITIALS_TITLE]) {
        cellLabelWidth = ViewFactory.initialsCellLabelWidth;
    }
    return cellLabelWidth;
}

- (void) buttonPressed:(UIButton*)sender {
    soundString = [sender.layer valueForKey:FINAL_KEY];
    [self performSegueWithIdentifier:SINGLE_SOUND_SEGUE_ID sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:SINGLE_SOUND_SEGUE_ID]) {
        SingleSoundViewController *viewController = [segue destinationViewController];
        [viewController setSoundString:soundString];
    }
}

@end
