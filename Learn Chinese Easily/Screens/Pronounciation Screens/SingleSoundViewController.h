//
//  SinglePronounciationViewController.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 22/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SingleSoundViewController : UIViewController <AVAudioPlayerDelegate>

@property (strong, nonatomic) AVAudioPlayer *soundPlayer;
@property (nonatomic, copy) NSString *soundString;

@end
