//
//  SecondViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 14/09/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "CategoryTableViewController.h"
#import "HSKListTableViewController.h"
#import "WordListTableViewController.h"

#import "ArrayFactory.h"
#import "CellFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"

#import "SizeConstants.h"
#import "StringConstants.h"

@interface HSKListTableViewController ()

@end

NSString *selectedHSKLevel;

@implementation HSKListTableViewController

@synthesize mainMenu;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self formatNavigationBar];
    mainMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) formatNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   ColorFactory.mainColor, NSForegroundColorAttributeName,
                                                                   FontFactory.mainFontMedium, NSFontAttributeName,nil];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowRadius = SHADOW_RADIUS;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.3;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ArrayFactory.listOfHSKLevels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HSK_TABLE_CELL_ID];
    NSInteger cellIndex = indexPath.row;
    NSString *titleString = [ArrayFactory.listOfHSKLevels objectAtIndex:cellIndex];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HSK_TABLE_CELL_ID];
    }
    
    if ([self isHeaderCellAtIndex:cellIndex]) {
        cell = [CellFactory createHeaderCell:cell withText:titleString];
    } else {
        cell = [CellFactory createNormalCell:cell withText:titleString];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger cellIndex = indexPath.row;
    if ([self isHeaderCellAtIndex:cellIndex]) {
        return HEADER_CELL_HEIGHT;
    } else {
        return NORMAL_CELL_HEIGHT;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedHSKLevel = [ArrayFactory.listOfHSKLevels objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:VOCAB_CATEGORY_SEGUE_ID sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:VOCAB_CATEGORY_SEGUE_ID]) {
        CategoryTableViewController *viewController = [segue destinationViewController];
        [viewController setHskLevel:selectedHSKLevel];
    }
}

- (BOOL) isHeaderCellAtIndex:(NSInteger)index {
    return [[ArrayFactory.listOfHSKLevels objectAtIndex:index] isEqualToString:HSK_HEADER_STRING];
}

@end
