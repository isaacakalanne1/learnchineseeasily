//
//  VocabularyListTableViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 24/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "WordListTableViewController.h"
#import "SingleWordViewController.h"
#import "WordTableViewCell.h"

#import "CellFactory.h"
#import "ColorFactory.h"
#import "FontFactory.h"
#import "Screen.h"

#import "SizeConstants.h"
#import "StringConstants.h"

NSMutableDictionary *wordData;
BOOL isEditing;

@interface WordListTableViewController ()

@end

@implementation WordListTableViewController

@synthesize wordListTableView;
@synthesize listOfWords;
@synthesize categoryName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isEditing = false;
    wordListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.rightBarButtonItem = [self createEditButton];
    
    listOfWords = [[NSMutableArray alloc] init];
    listOfWords = [self getSavedDataForCategory:categoryName];
}

- (NSMutableArray *) getSavedDataForCategory:(NSString *)category {
    NSUserDefaults *savedData = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrayOfSavedWords = [[NSMutableArray alloc] init];
    NSMutableArray *arrayOfCategoryObjects = [savedData objectForKey:HSK1_PRACTICE_LIST_KEY];
    
    for (int i = 0 ; i < arrayOfCategoryObjects.count ; i++) {
        NSDictionary *categoryObject = [arrayOfCategoryObjects objectAtIndex:i];
        if ([[categoryObject objectForKey:CATEGORY_KEY] isEqualToString:category]) {
            arrayOfSavedWords = [categoryObject objectForKey:ARRAY_OF_WORDS_KEY];
            arrayOfSavedWords = [NSMutableArray arrayWithArray:arrayOfSavedWords];
        }
    }
    return arrayOfSavedWords;
}

- (void) saveDataForCategory:(NSString *)category {
    NSUserDefaults *savedData = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayOfCategoryObjects = [savedData objectForKey:HSK1_PRACTICE_LIST_KEY];
    
    for (int i = 0 ; i < arrayOfCategoryObjects.count ; i++) {
        NSMutableDictionary *categoryObject = [arrayOfCategoryObjects objectAtIndex:i];
        categoryObject = [NSMutableDictionary dictionaryWithDictionary:categoryObject];
        
        if ([[categoryObject objectForKey:CATEGORY_KEY] isEqualToString:category]) {
            [categoryObject setObject:listOfWords forKey:ARRAY_OF_WORDS_KEY];
            
            arrayOfCategoryObjects = [NSMutableArray arrayWithArray:arrayOfCategoryObjects];
            [arrayOfCategoryObjects replaceObjectAtIndex:i withObject:categoryObject];
        }
    }
    [savedData setObject:arrayOfCategoryObjects forKey:HSK1_PRACTICE_LIST_KEY];
    [savedData synchronize];
}

- (UIBarButtonItem *) createEditButton {
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:EDIT_PRACTICE_LIST style:UIBarButtonItemStylePlain target:self action:@selector(editButtonPressed)];
    return editButton;
}

- (void) editButtonPressed {
    isEditing = !isEditing;
    wordListTableView.allowsMultipleSelection = isEditing;
    [self updateViews];
    if (isEditing) {
        [self selectWordsBeingPracticed];
    } else {
        [self saveDataForCategory:categoryName];
    }
}

- (void) selectWordsBeingPracticed {
    for (int i = 0 ; i < listOfWords.count ; i++) {
        NSDictionary *wordData = [listOfWords objectAtIndex:i];
        if ([[wordData objectForKey:IS_PRACTICING_KEY] boolValue] == TRUE) {
            NSLog(@"%d is i!", i);
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [wordListTableView selectRowAtIndexPath:indexPath animated:FALSE scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

- (void) updateViews {
    self.navigationItem.rightBarButtonItem.title = [self setEditButtonTitle];
    [wordListTableView reloadData];
}

- (NSString *) setEditButtonTitle {
    if (isEditing) {
        return SAVE;
    } else {
        return EDIT_PRACTICE_LIST;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listOfWords.count;
}

- (WordTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VOCAB_TABLE_CELL_ID forIndexPath:indexPath];
    NSDictionary *wordData = [listOfWords objectAtIndex:indexPath.row];
    NSString *titleString = [wordData objectForKey:ENGLISH_KEY];
    
    if (cell == nil) {
        cell = [[WordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:VOCAB_TABLE_CELL_ID];
    }
    
    cell.selectionStyle = [self setCellSelectionStyle];
    [cell setSelected:[self isPracticingWordAtIndex:indexPath.row]];
    cell = [CellFactory createNormalCell:cell withText:titleString];
    
    return cell;
}

- (UITableViewCellSelectionStyle) setCellSelectionStyle {
    if (isEditing) {
        return UITableViewCellSelectionStyleDefault;
    } else {
        return UITableViewCellSelectionStyleNone;
    }
}

- (BOOL) isPracticingWordAtIndex:(NSInteger)index {
    NSDictionary *wordData = [listOfWords objectAtIndex:index];
    return [[wordData objectForKey:IS_PRACTICING_KEY] boolValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    wordData = [listOfWords objectAtIndex:indexPath.row];
    if (isEditing) {
        [self updateIsPracticingBoolAtIndex:indexPath.row withPracticingStatus:TRUE];
    } else {
        [self performSegueWithIdentifier:SINGLE_VOCAB_SEGUE_ID sender:self];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    wordData = [listOfWords objectAtIndex:indexPath.row];
    [self updateIsPracticingBoolAtIndex:indexPath.row withPracticingStatus:FALSE];
}

- (void) updateIsPracticingBoolAtIndex:(NSInteger)index withPracticingStatus:(BOOL)isPracticing {
    NSMutableDictionary *newWordData = [[NSMutableDictionary alloc] init];
    [newWordData addEntriesFromDictionary:wordData];
    [newWordData setObject:[NSNumber numberWithBool:isPracticing] forKey:IS_PRACTICING_KEY];
    [listOfWords replaceObjectAtIndex:index withObject:newWordData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return NORMAL_CELL_HEIGHT;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SINGLE_VOCAB_SEGUE_ID]) {
        SingleWordViewController *viewController = [segue destinationViewController];
        [viewController setWordData:wordData];
    }
}

@end
