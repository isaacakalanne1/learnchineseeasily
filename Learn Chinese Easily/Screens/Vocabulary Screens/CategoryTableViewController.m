//
//  VocabularyTableViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 24/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "CategoryTableViewController.h"
#import "WordListTableViewController.h"

#import "ColorFactory.h"
#import "ArrayFactory.h"
#import "FontFactory.h"
#import "CellFactory.h"

#import "StringConstants.h"
#import "SizeConstants.h"

NSArray *listOfCategories;
NSString *categoryName;

@interface CategoryTableViewController ()

@end

@implementation CategoryTableViewController

@synthesize categoryMenu;
@synthesize hskLevel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createListOfCategories];
    [self formatNavigationBar];
    [self formatMenu];
}

- (void) createListOfCategories {
    if ([hskLevel isEqualToString:HSK1_STRING]) {
        listOfCategories = [ArrayFactory listOfCategoryNamesForHskLevel:hskLevel];
        categoryMenu.allowsSelection = true;
    } else {
        listOfCategories = [ArrayFactory hskComingSoon];
        categoryMenu.allowsSelection = false;
    }
}

- (void) formatNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   ColorFactory.mainColor, NSForegroundColorAttributeName,
                                                                   FontFactory.mainFontMedium, NSFontAttributeName,nil];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.navigationController.navigationBar.layer.shadowRadius = SHADOW_RADIUS;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.3;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 0);
}

- (void) formatMenu {
    categoryMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
    [categoryMenu reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listOfCategories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CATEGORY_TABLE_CELL_ID];
    NSString *cellTitle = [listOfCategories objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CATEGORY_TABLE_CELL_ID];
    }
    
    cell = [CellFactory createNormalCell:cell withText:cellTitle];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    categoryName = [listOfCategories objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:VOCAB_LIST_SEGUE_ID sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([[segue identifier] isEqualToString:VOCAB_LIST_SEGUE_ID]) {
         WordListTableViewController *viewController = [segue destinationViewController];
         [viewController setCategoryName:categoryName];
     }
}

@end
