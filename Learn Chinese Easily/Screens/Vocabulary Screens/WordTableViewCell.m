//
//  WordTableViewCell.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 25/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "WordTableViewCell.h"

#import "ColorFactory.h"
#import "Screen.h"
#import "SizeConstants.h"
#import "StringConstants.h"

BOOL isCheckmarkShown = false;

@implementation WordTableViewCell

@synthesize circle;
@synthesize checkmark;

- (void)awakeFromNib {
    [super awakeFromNib];
    // TODO Check why this code isn't running
    
    circle = [self createCircle];
    checkmark = [self createCheckmark];
    [self toggleCheckmark:isCheckmarkShown];
    
    [circle addSubview:checkmark];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    isCheckmarkShown = selected;
    if (self.selectionStyle != UITableViewCellSelectionStyleNone) {
        [self toggleCheckmark:isCheckmarkShown];
    }
}

- (void) toggleCheckmark:(BOOL)isShown {
    if (isShown) {
        [self addSubview:circle];
    } else {
        [circle removeFromSuperview];
    }
    [self setHighlighted:isShown];
}

- (UIView *) createCircle {
    UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(Screen.width - CHECKMARK_SIZE - (NORMAL_CELL_HEIGHT - CHECKMARK_SIZE)/2, NORMAL_CELL_HEIGHT/2 - CHECKMARK_SIZE/2, CHECKMARK_SIZE, CHECKMARK_SIZE)];
    circle.layer.cornerRadius = CHECKMARK_SIZE/2;
    circle.layer.masksToBounds = YES;
    circle.backgroundColor = ColorFactory.mainColor;
    return circle;
}

- (UILabel *) createCheckmark {
    UILabel *checkmark = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, circle.bounds.size.width, circle.bounds.size.height)];
    checkmark.text = CHECKMARK;
    checkmark.textAlignment = NSTextAlignmentCenter;
    checkmark.textColor = UIColor.whiteColor;
    return checkmark;
}

@end
