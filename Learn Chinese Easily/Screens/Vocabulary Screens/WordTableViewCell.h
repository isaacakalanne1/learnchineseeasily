//
//  WordTableViewCell.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 25/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WordTableViewCell : UITableViewCell

@property (strong, nonatomic) UIView *circle;
@property (strong, nonatomic) UILabel *checkmark;

@end
