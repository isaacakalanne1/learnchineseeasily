//
//  SingleVocabularyViewController.m
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 25/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import "SingleWordViewController.h"

#import "ColorFactory.h"
#import "FontFactory.h"
#import "Screen.h"
#import "ViewFactory.h"

#import "SizeConstants.h"
#import "StringConstants.h"

NSString *wordId;
NSString *hanziString;
NSString *hanziString;
NSString *pinyinString;
NSString *_definitionString;

@interface SingleWordViewController ()

@end

@implementation SingleWordViewController

@synthesize wordData;
@synthesize wordLabel;
@synthesize hanziLabel;
@synthesize pinyinLabel;
@synthesize definitionLabel;
@synthesize playWordButton;
@synthesize wordAudioPlayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setStrings];
    [self createWordAudioPlayer];
    [self createInterface];
    [self playWord];
}

- (void) setStrings {
    wordId = [[wordData objectForKey:ID_KEY] stringValue];
    hanziString = [wordData objectForKey:HANZI_KEY];
    pinyinString = [wordData objectForKey:PINYIN_KEY];
    _definitionString = [wordData objectForKey:ENGLISH_KEY];
}

- (void) createInterface {
    hanziLabel = [ViewFactory createDefaultLabelWithText:hanziString andFrame:CGRectMake(0, MEDIUM_MARGIN, Screen.width, 27)];
    hanziLabel.font = FontFactory.mainFontLarge;
    
    pinyinLabel = [ViewFactory createDefaultLabelWithText:pinyinString andFrame:CGRectMake(0, [ViewFactory getBottom:hanziLabel] + MEDIUM_MARGIN, Screen.width, 20)];
    
    definitionLabel = [ViewFactory createDefaultLabelWithText:_definitionString andFrame:CGRectMake(0, [ViewFactory getBottom:pinyinLabel] + SMALL_MARGIN, Screen.width, 150)];
    [definitionLabel sizeToFit];
    definitionLabel.frame = CGRectMake(definitionLabel.frame.origin.x, definitionLabel.frame.origin.y, Screen.width, definitionLabel.frame.size.height);
    
    [self createPlayWordButton];
    [self addSubviews];
}

- (void) createPlayWordButton {
    playWordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [playWordButton setTitle:PLAY_SOUND_STRING forState:UIControlStateNormal];
    [playWordButton setTitleColor:ColorFactory.mainColor forState:UIControlStateNormal];
    [playWordButton.titleLabel setFont:FontFactory.mainFontMedium];
    [playWordButton addTarget:self action:@selector(playWord) forControlEvents:UIControlEventTouchUpInside];
    playWordButton.frame = CGRectMake(0, Screen.height - 150, Screen.width, HEIGHT_OF_BUTTON);
}

- (void) addSubviews {
    [self.view addSubview:hanziLabel];
    [self.view addSubview:pinyinLabel];
    [self.view addSubview:definitionLabel];
    [self.view addSubview:playWordButton];
}

- (void) createWordAudioPlayer {
    NSString *pathOfSoundFile = [[NSBundle mainBundle] pathForResource:wordId ofType:MP3_TYPE];
    if (pathOfSoundFile != nil) {
        NSURL *urlOfSoundFile = [NSURL fileURLWithPath:pathOfSoundFile];
        NSError *error;
        wordAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlOfSoundFile error:&error];
    } else {
        NSLog(FILE_DOESNT_EXIST);
    }
}

- (void) playWord {
    if (!wordAudioPlayer.isPlaying) {
        [wordAudioPlayer play];
    }
}

@end
