//
//  SingleVocabularyViewController.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 25/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SingleWordViewController : UIViewController <AVAudioPlayerDelegate>

@property (strong, nonatomic) NSDictionary *wordData;
@property (strong, nonatomic) UILabel *wordLabel;
@property (strong, nonatomic) UILabel *hanziLabel;
@property (strong, nonatomic) UILabel *pinyinLabel;
@property (strong, nonatomic) UILabel *definitionLabel;
@property (strong, nonatomic) UIButton *playWordButton;

@property (strong, nonatomic) AVAudioPlayer *wordAudioPlayer;

@end
