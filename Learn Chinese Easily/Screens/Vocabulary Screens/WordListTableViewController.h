//
//  VocabularyListTableViewController.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 24/10/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WordListTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *wordListTableView;
@property (nonatomic) NSMutableArray *listOfWords;
@property (nonatomic, copy) NSString *categoryName;

@end
