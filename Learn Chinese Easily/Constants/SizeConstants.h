//
//  SizeConstants.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 01/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#define         SMALL_MARGIN            10
#define         MEDIUM_MARGIN           20
#define         LARGE_MARGIN            30

#define         WIDTH_OF_BUTTON         150
#define         HEIGHT_OF_BUTTON        30

#define         SHADOW_RADIUS           15

// TABLE VIEW CELLS
#define         HEADER_CELL_HEIGHT      30
#define         NORMAL_CELL_HEIGHT      45
#define         CHECKMARK_SIZE          30

// LABEL
#define         LARGE_LABEL_HEIGHT      30
#define         MEDIUM_LABEL_HEIGHT     20
