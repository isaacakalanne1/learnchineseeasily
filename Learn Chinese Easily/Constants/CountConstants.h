//
//  CountConstants.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 24/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

#define         NUMBER_OF_FINALS_TABLE_ROWS            10
#define         NUMBER_OF_INITIALS_TABLE_ROWS          4
