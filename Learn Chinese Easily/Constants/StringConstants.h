//
//  StringConstants.h
//  Learn Chinese Easily
//
//  Created by Isaac Akalanne on 01/11/2019.
//  Copyright © 2019 Believe And Succeed Apps. All rights reserved.
//

// KEYS
#define     SOUND_KEY               @"Sound"
#define     PINYIN_KEY              @"Pinyin"
#define     HANZI_KEY               @"Hanzi"
#define     ENGLISH_KEY             @"English"
#define     DEFINITION_KEY          @"Definition"
#define     PRONOUNCIATION_KEY      @"Pronounciation"
#define     ID_KEY                  @"Id"
#define     CATEGORY_KEY            @"Category"
#define     ARRAY_OF_WORDS_KEY      @"ArrayOfWords"
#define     ENGLISH_KEY             @"English"
#define     FINAL_KEY               @"finalString"
#define     IS_PRACTICING_KEY       @"isPracticing"
#define     HSK1_PRACTICE_LIST_KEY  @"hsk1PracticeList"

// PLIST
#define     HSK1_PATH               @"hsk1"
#define     HSK2_PATH               @"hsk2"
#define     HSK3_PATH               @"hsk3"
#define     HSK4_PATH               @"hsk4"
#define     HSK5_PATH               @"hsk5"
#define     HSK6_PATH               @"hsk6"
#define     PRONOUNCIATION_PATH     @"pronounciation"

// FONTS
#define     AVENIR_FONT             @"Avenir"

// NSBUNDLE
#define     PLIST_TYPE              @"plist"
#define     MP3_TYPE                @"mp3"

// MISC
#define     FILE_DOESNT_EXIST       @"Whoops, this file doesn't exist!"

// SEGUES
#define     PRACTICE_SEGUE_ID       @"openPracticeViewController"
#define     VOCAB_CATEGORY_SEGUE_ID @"performCategoryTableViewSegue"
#define     VOCAB_LIST_SEGUE_ID     @"performVocabularyListTableViewSegue"
#define     SINGLE_VOCAB_SEGUE_ID   @"performSingleVocabularySegue"
#define     SINGLE_SOUND_SEGUE_ID   @"performSinglePronounciationSegue"

// CELL IDS
#define     HSK_TABLE_CELL_ID       @"vocabTableViewCellIdentifier"
#define     VOCAB_TABLE_CELL_ID     @"vocabularyListTableViewCellIdentifier"
#define     CATEGORY_TABLE_CELL_ID  @"categoryTableViewCellIdentifier"

// STRINGS
#define     START_STRING        @"Start"
#define     SHOW_ANSWER_STRING  @"Show Answer"
#define     PLAY_SOUND_STRING   @"Play Sound"
#define     CHECKMARK           @"✓"
#define     CROSS               @"✗"
#define     HSK1_STRING         @"HSK1"
#define     HSK2_STRING         @"HSK2"
#define     HSK3_STRING         @"HSK3"
#define     HSK4_STRING         @"HSK4"
#define     HSK5_STRING         @"HSK5"
#define     HSK6_STRING         @"HSK6"
#define     HSK_HEADER_STRING   @"LEVELS"
#define     FINALS_TITLE        @"FINALS"
#define     INITIALS_TITLE      @"INITIALS"
#define     EDIT_PRACTICE_LIST  @"Edit Practice List"
#define     SAVE                @"Save"
#define     EMPTY_STRING        @""
